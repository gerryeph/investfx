from django import views
from django.contrib.auth import authenticate
from django.contrib.auth import login as auth_login
from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.hashers import make_password
from django.db.models import F
from .forms import *
from app.models import *
from django.http import HttpResponse
from django.shortcuts import render
import requests

# Create your views here.

def login(request):
    login_form = LoginForm
    context ={
        "title": "Log in | Technical Arena",
        "login_form": login_form
    }
    return render(request, "app/backend/sign-in/signin.html", context)

def register(request):
    registration_form = RegistrationForm
    context = {
        "title": "Register | Technical Arena",
        "registration_form": registration_form
    }
    return render(request, "app/backend/sign-in/registration.html", context)

@csrf_exempt
def login_action(request):
    if request.method == "GET":
        print("Invalid Request")
        login_form = LoginForm
        context = {
            "title": "Log in | Technical Arena",
            "login_form": login_form
        }
        return redirect('ta_login')
    elif request.method == "POST":

        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        print(username)
        print(password)

        if user is not None:
            auth_login(request, user)
            if not request.user.is_authenticated:
                login_form = LoginForm
                context = {
                    "title": "Log in | Technical Arena",
                    "login_form": login_form
                }
                return redirect('ta_login')
            else:
                userprofile_logging_in = UserProfile.objects.filter(user=user)
                print(userprofile_logging_in)

                if userprofile_logging_in.first().type == "Analyst":
                    request.session['user_id'] = user.id
                    request.session['user_type'] = "Analyst"

                    session_stored_user_type = request.session.get("user_type")
                    session_stored_user_id = request.session.get("user_id")

                    print(session_stored_user_id)
                    return redirect('technical-arena-analyst-dashboard')
                elif userprofile_logging_in.first().type == "Customer":
                    request.session['user_id'] = user.id
                    request.session['user_type'] = "Analyst"

                    session_stored_user_type = request.session.get("user_type")
                    session_stored_user_id = request.session.get("user_id")

                    print(session_stored_user_id)
                    return redirect('customer-dashboard')

                else:
                    return redirect('ta_login')
        else:
            login_form = LoginForm
            context = {
                "title": "Log in | Technical Arena",
                "login_form": login_form
            }
            return redirect('ta_login')


@csrf_exempt
def registration_action(request):
    if request.method == "GET":
        print("Invalid Request")
        registration_form = RegistrationForm
        context = {
            "title": "Registe | Technical Arena",
            "registration_form": registration_form
        }
        return redirect('ta_register')
    elif request.method == "POST":
        print("Sum up the Registration action function")

        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        phone_number = request.POST['phone_number']
        password = request.POST['password']

        try:
            existing_user = User.objects.get(username=username)
            registration_form = RegistrationForm(request.POST)
            context = {
                "title": "Register | Technical Arena",
                "registration_form": registration_form,
                "username_500": "Username already exists, please choose another username"
            }
            return redirect('ta_register')
        except ObjectDoesNotExist:
            profile_type = "Customer"
            form = RegistrationForm(request.POST)
            user = User.objects.create_user(request.POST)
            user.first_name = first_name
            user.last_name = last_name
            user.username = username
            user.password = make_password(password)
            user.email = email
            user.save()

            def get_created_profile(user):
                created_profile = UserProfile.objects.filter(user=user)
                return created_profile.first(), created_profile.exists()

            if get_created_profile(user)[1]:
                UserProfile.objects.filter(user=user).update(type=profile_type, phone_number=phone_number)
                print("Found and updated")
            else:
                print("no found profile")

            return redirect('ta_login')


@csrf_exempt
def logout_action(request):
    auth_logout(request)
    return redirect('ta_login')


def home(request):
    
    most_viewed_analysis = Analysis.objects.order_by('-view_counts')[:2]
    most_viewed_articles = Article.objects.order_by('-view_counts')[:2]
    latest_analysis = Analysis.objects.all().latest('posted_on')

    context = {
        "title": "Technical Arena - InvestFX",
        "most_viewed_analysis": most_viewed_analysis,
        "most_viewed_articles": most_viewed_articles,
        "latest_analysis": latest_analysis
    }
    return render(request, "app/backend/technical_arena/home.html", context)


@login_required(login_url='ta_login')
def analyst_dashboard(request):

    currency_pairs = CurrencyPair.objects.all()

    context = {
        "title": "Analyst Dashboard | Technical Arena",
        "company_name": "InvestFX",
        "arena": "Analyst Arena",
        "currency_pairs": currency_pairs
    }
    return render(request, "app/backend/dashboard/analyst_dashboard.html", context)

@login_required(login_url='ta_login')
def customer_dashboard(request):

    analysis = Analysis.objects.all().order_by('-posted_on')
    articles = Article.objects.all().order_by('-posted_on')

    context = {
        "title": "Customer Dashboard | Technical Arena",
        "company_name": "InvestFX",
        "arena": "Technical Arena",
        "analysis": analysis,
        "articles": articles
    }
    return render(request, "app/backend/dashboard/customer_dashboard.html", context)

@login_required(login_url='ta_login')
def article_details(request, pk):

    article = Article.objects.get(id=pk)
    article.view_counts = F('view_counts') + 1
    article.save()

    context = {
        "title": "{} | Article".format(article.title),
        "company_name": "InvestFX",
        "arena": "Technical Arena",
        "article": article
    }
    return render(request, "app/backend/dashboard/article_detail.html", context)

@login_required(login_url='ta_login')
def analysis_details(request, pk):

    analysis = Analysis.objects.get(id=pk)
    analysis.view_counts = F('view_counts') + 1
    analysis.save()

    context = {
        "title": "{} | Analysis".format(analysis.title),
        "company_name": "InvestFX",
        "arena": "Technical Arena",
        "analysis": analysis
    }
    return render(request, "app/backend/dashboard/analysis_detail.html", context)

