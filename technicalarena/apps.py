from django.apps import AppConfig


class TechnicalarenaConfig(AppConfig):
    name = 'technicalarena'
