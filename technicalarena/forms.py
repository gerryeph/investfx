from .models import *
from django import forms
from django.contrib.auth.models import User


class LoginForm(forms.Form):
    username = forms.CharField(
        label='Username',
        required=True,
        help_text='Username',
        error_messages={
            'required': 'Please fill this'
        }
    )

    password = forms.CharField(
        label='Password',
        required=True,
        help_text='Password',
        error_messages={
            'required': 'Please fill this'
        },
        widget=forms.PasswordInput
    )

class RegistrationForm(forms.Form):
    first_name = forms.CharField(
        label='First name',
        required=True,
        help_text='First Name',
        error_messages={
            'required': 'Please fill this'
        }
    )
    
    last_name = forms.CharField(
        label='Last name',
        required=True,
        help_text='Last Name',
        error_messages={
            'required': 'Please fill this'
        }
    )

    username = forms.CharField(
        label='Username',
        required=True,
        help_text='Username',
        error_messages={
            'required': 'Please fill this'
        }
    )

    email = forms.CharField(
        label='Email',
        required=True,
        help_text='Email',
        error_messages={
            'required': 'Please fill this'
        },
        widget=forms.EmailInput
    )

    password = forms.CharField(
        label='Password',
        required=True,
        help_text='Password',
        error_messages={
            'required': 'Please fill this'
        },
        widget=forms.PasswordInput
    )

    phone_number = forms.CharField(
        label='Phone number',
        required=True,
        help_text='Phone Number',
        error_messages={
            'required': 'Please fill this'
        }
    )