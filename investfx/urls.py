"""investfx URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from app import views as AppViews
from technicalarena import views as TechnicalArenaViews
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', AppViews.index, name="index"),
    url(r'technical-arena/login/', TechnicalArenaViews.login, name="ta_login"),
    url(r'technical-arena/register/', TechnicalArenaViews.register, name="ta_register"),
    url(r'technical-arena/login-action/', TechnicalArenaViews.login_action, name="login_action"),
    url(r'technical-arena/registration-action/', TechnicalArenaViews.registration_action, name="registration_action"),
    url(r'technical-arena/logout-action/', TechnicalArenaViews.logout_action, name="logout_action"),
    url(r'technical-arena/home/', TechnicalArenaViews.home, name="ta_home"),
    url(r'technical-arena/analyst-dashboard/', TechnicalArenaViews.analyst_dashboard, name="technical-arena-analyst-dashboard"),
    url(r'technical-arena/customer-dashboard/', TechnicalArenaViews.customer_dashboard, name="customer-dashboard"),
    url(r'technical-arena/article-detail-view/(?P<pk>[-\w]+)/$', TechnicalArenaViews.article_details, name='article-details'),
    url(r'technical-arena/analysis-detail-view/(?P<pk>[-\w]+)/$', TechnicalArenaViews.analysis_details, name='analysis-details'),

]
