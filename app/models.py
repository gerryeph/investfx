# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from statistics import mode

from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import User
from django.db import models
from ckeditor.fields import RichTextField
# Create your models here.



class UserProfile(models.Model):
    USER_TYPES = (
        ("Admin", "Admin"),
        ("Customer", "Customer"),
        ("Staff", "Staff"),
        ("Analyst", "Analyst")
    )

    BLACKLISTED_CHOICES = (
        ('Yes', 'Yes'),
        ('No', 'No')
    )
    user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
    type = models.CharField(max_length=50, null=False, choices=USER_TYPES)
    phone_number = models.CharField(max_length=50, null=True)
    email_address = models.CharField(max_length=100, null=True, blank=True)
    dob = models.DateField(null=True, blank=True)
    blacklist_status = models.CharField(max_length=50, null=True, choices=BLACKLISTED_CHOICES, default="No")

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs,):
        if created:
            UserProfile.objects.create(user=instance)

    def __str__(self):
        return self.user.username



class CurrencyPair(models.Model):
    name = models.CharField(max_length=500, null=True, blank=True)
    symbol = models.CharField(max_length=20, null=True, blank=True)
    description = models.TextField(max_length=100, null=True, blank=True)
    analysis_counts = models.IntegerField(null=True, blank=True,)
    status = models.BooleanField(default=True)
    added_on = models.DateTimeField(auto_now=True)


    def __str__(self):
        return self.symbol


class Analysis(models.Model):
    TRADE_BIAS_CHOICES = (
        ('SELL', 'SELL'),
        ('BUY', 'BUY'),
        ('NEUTRAL', 'NEUTRAL')
    )
    TIME_FRAME_CHOICES = (
        ('15m', '15m'),
        ('30m', '30m'),
        ('1H', '1H'),
        ('4H', '4H'),
        ('D', 'D'),
        ('W', 'W'),
        ('M', 'M'),
    )
    CATEGORY_CHOICES = (
        ('Crypto', 'Crypto'),
        ('Forex', 'Forex'),
    )
    title = models.CharField(max_length=500, null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES, null=True, default="Forex")
    trade_bias = models.CharField(max_length=50, choices=TRADE_BIAS_CHOICES, null=True)
    time_frame = models.CharField(max_length=50, null=True, blank=True, choices=TIME_FRAME_CHOICES)
    currency_pair_id = models.ForeignKey(CurrencyPair, on_delete=models.CASCADE, null=True, blank=True)
    view_counts = models.IntegerField(blank=True, default=0)
    video_link = models.CharField(max_length=500, null=True, blank=True)
    posted_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    posted_on = models.DateTimeField(auto_now=False)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.currency_pair_id.name


class Article(models.Model):
    CATEGORY_CHOICES = (
        ('Crypto', 'Crypto'),
        ('Forex', 'Forex'),
        ('Pyschology', 'Pyschology'),
    )
    title = models.CharField(max_length=500, null=True, blank=True)
    description = RichTextField(null=True, blank=True)
    category = models.CharField(max_length=50, choices=CATEGORY_CHOICES, null=True, default="Forex")
    currency_pair_id = models.ForeignKey(CurrencyPair, on_delete=models.CASCADE, null=True, blank=True)
    view_counts = models.IntegerField(blank=True, default=0)
    posted_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    posted_on = models.DateTimeField(auto_now=False)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title


class Membership(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    fee = models.CharField(max_length=500, null=True, blank=True)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    created_on = models.DateTimeField(auto_now=False)
    updated_on = models.DateTimeField(auto_now=False)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class Course(models.Model):
    name = models.CharField(max_length=100, null=True, blank=True)
    description = models.CharField(max_length=500, null=True, blank=True)
    fee = models.CharField(max_length=500, null=True, blank=True)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    created_on = models.DateTimeField(auto_now=False)
    updated_on = models.DateTimeField(auto_now=False)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class LegalPart(models.Model):
    LEGAL_TYPE_CHOICES = (
        ("Privacy Policy", "Privacy Policy"),
        ("Terms & Conditions", "Terms & Conditions"),
        ("Disclaimer", "Disclaimer")
    )
    name = models.CharField(max_length=100, null=True, blank=True)
    legal_type = models.CharField(max_length=100, choices=LEGAL_TYPE_CHOICES, null=True, blank=True)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    created_on = models.DateTimeField(auto_now=True)
    updated_on = models.DateTimeField(auto_now=True)
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name


class LegalContent(models.Model):
    legal_part_id = models.ForeignKey(LegalPart, on_delete=models.CASCADE, null=True, blank=True)
    content = models.TextField(null=True, blank=True)
    created_by = models.ForeignKey(UserProfile, on_delete=models.CASCADE, null=True, blank=True)
    created_on = models.DateTimeField(auto_now=False)
    updated_on = models.DateTimeField(auto_now=False)
