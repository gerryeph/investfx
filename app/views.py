# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.http import HttpResponse
from django.shortcuts import render
import requests

import tradingeconomics as te

# Create your views here.

def index(request):
    context = {
        "title": "Welcome to InvestFX",
    }
    return render(request, "app/frontend/index.html", context)


