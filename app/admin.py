# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.models import *
from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin
from django.contrib.admin import AdminSite
import datetime

current_date_time = datetime.datetime.now()


AdminSite.enable_nav_sidebar = False

# Register your models here.

class CurrencyPairResource(resources.ModelResource):
    class Meta:
        model = CurrencyPair

class UserProfileAdmin(admin.ModelAdmin):
    fields = ['user', 'type', 'phone_number', 'email_address', 'dob', 'blacklist_status']
    list_display = ['user', 'type', 'phone_number', 'email_address', 'dob', 'blacklist_status']


class CurrencyPairAdmin(ImportExportModelAdmin):
    fields = ['name', 'symbol', 'description', 'analysis_counts', 'status']
    list_display = ['name', 'symbol', 'description', 'analysis_counts', 'status']
    readonly_fields = ['added_on']


class AnalysisAdmin(admin.ModelAdmin):
    fields = ['title', 'description', 'category','trade_bias', 'time_frame', 'view_counts' ,'currency_pair_id', 'video_link','posted_by', 'posted_on', 'updated_on']
    list_display = ['title', 'description', 'category','trade_bias', 'time_frame', 'view_counts' ,'currency_pair_id', 'video_link','posted_by', 'posted_on', 'updated_on']
    readonly_fields = ['posted_on', 'updated_on']

    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.posted_by:
            user_profile = UserProfile.objects.get(user_id=user)
            instance.posted_by = user_profile
            instance.posted_on = current_date_time
        user_profile = UserProfile.objects.get(user_id=user)
        instance.posted_by = user_profile
        instance.posted_on = current_date_time

        instance.save()
        form.save_m2m()

        return instance


class ArticleAdmin(admin.ModelAdmin):
    fields = ['title', 'description', 'category', 'currency_pair_id', 'view_counts','posted_by', 'posted_on', 'updated_on']
    list_display = ['title', 'description', 'category','currency_pair_id', 'view_counts','posted_by', 'posted_on', 'updated_on']
    readonly_fields = ['posted_on', 'updated_on']

    def save_model(self, request, instance, form, change):
        user = request.user
        instance = form.save(commit=False)
        if not change or not instance.posted_by:
            user_profile = UserProfile.objects.get(user_id=user)
            instance.posted_by = user_profile
            instance.posted_on = current_date_time
        user_profile = UserProfile.objects.get(user_id=user)
        instance.posted_by = user_profile
        instance.posted_on = current_date_time

        instance.save()
        form.save_m2m()

        return instance


class MembershipAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'fee', 'created_by', 'created_on', 'updated_on', 'status']
    list_display = ['name', 'description', 'fee', 'created_by', 'created_on', 'updated_on', 'status']


class CourseAdmin(admin.ModelAdmin):
    fields = ['name', 'description', 'fee', 'created_by', 'created_on', 'updated_on', 'status']
    list_display = ['name', 'description', 'fee', 'created_by', 'created_on', 'updated_on', 'status']


class LegalPartAdmin(admin.ModelAdmin):
    fields = ['name', 'legal_type', 'created_by', 'created_on', 'updated_on', 'status']
    list_display = ['name', 'legal_type', 'created_by', 'created_on', 'updated_on', 'status']


class LegalContentAdmin(admin.ModelAdmin):
    fields = ['legal_part_id', 'content', 'created_by', 'created_on', 'updated_on',]
    list_display = ['legal_part_id', 'content', 'created_by', 'created_on', 'updated_on',]


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(CurrencyPair, CurrencyPairAdmin)
admin.site.register(Analysis, AnalysisAdmin)
admin.site.register(Article, ArticleAdmin)
admin.site.register(Membership, MembershipAdmin)
admin.site.register(Course, CourseAdmin)
admin.site.register(LegalPart, LegalPartAdmin)
admin.site.register(LegalContent, LegalContentAdmin)